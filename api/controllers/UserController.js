/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var passport = require('passport');
const jwToken = require('jsonwebtoken');
tokenSecret = process.env.TOKEN_SECRET;

module.exports = {
	signin: async function(req, res){
		await passport.authenticate('local', {session: false}, function(err, user, info){
			//response delivered if an error occurs during login or the user doesn't exist
			if((err) || (!user)){
				return  res.status(403).send({message: info.message, type:'error'});
			}

			//token set for user that succesfully logs in
			const token = jwToken.sign(
				user,
				tokenSecret
				);

			//response for succesful login
			return res.status(200).send({user, token, message: info.message, type:'success'});

		}) (req, res)
	},

	register: function(req, res){
		let details = {
			emailAddress : req.body.emailAddress,
			firstName : req.body.firstName,
			lastName : req.body.lastName,
			address: req.body.address,
			phone : req.body.phone,
			password : req.body.password
		}
		
		//create user if the user does not exist (based on the email address)
		User.findOrCreate({emailAddress: details.emailAddress}, details)
		.exec(async(err, userExists, createUser)=>{
			if(err){
				return res.status(403).send({type: 'error', message: 'An Error occured while creating user. Try again later'});
			}
			if(createUser){
				//response delivered if user registers successfully
				return res.status(201).send({
					message:'Created Successfully', type:'success'
				})
			}else{
				//response delivered if user is already registered
				return res.status(409).send(
					{message:'User already exists', type:'error'});
			}
		})
	},

	allUsers: function(req, res){
		User.find().populate('order')
		.then(users=>{
			return res.json(users)
		})
	},

	findUser: function(req, res){
		User.find({emailAddress: req.param('emailAddress')}).populate('order')
		.then(( user)=>{
			return res.json(user);
		})
	}
}  