/**
 * LaptopController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  items: async function(req, res){
  	let item = {
  		name : req.body.name,
  		price: req.body.price,
  		quantity: req.body.quantity,
      owner: req.body.owner
  	}
  	
  	await Laptop.create( item );

    var twilio = require('twilio');
        var accountSid = process.env.TWILIO_ACCOUNT_SID;
        var authToken = process.env.TWILIO_AUTH_TOKEN
        var client = new twilio(accountSid, authToken);

        client.messages.create({
          body: `Hello ${req.body.user},  your order has beeen created. Thanks for your patronage`,
          to: `+234${req.body.phone}`,
          from: '+15867880990'
        })
        .then(message=> console.log('Twilio - ', message.sid));


        const AWS = require('aws-sdk');

        AWS.config.update({
          accessKeyId: process.env.AWS_ACCESSKEY_ID,
          secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
          region: 'us-east-1'
        });

        const ses = new AWS.SES();
        const params = {
          Destination: {
            ToAddresses: [req.body.email]
          },
          ConfigurationSetName: 'Feedback',
          Message: {
            Body: {
              Html: {
                Charset: 'UTF-8',
                Data: `<html><body>Hello ${req.body.user}, 
                  Your order for <b><u> ${req.body.name} </u></b> has been created. 

                Thanks for your patronage!!</body></html>`
              },
              Text: {
                Charset: 'UTF-8',
                Data: 'Order description'
              }
            },
            Subject: {
              Charset: 'UTF-8',
              Data: 'Order Confirmation'
            }
          },
          Source: 'jesax013@gmail.com'
        }

        const sendEmail = ses.sendEmail(params).promise();

        sendEmail
          .then(data => {
            console.log('Email submitted to SES', data);
          })
          .catch(error => {
            console.log(error)
          }); 


     return res.status(201).send('Order created');

	}

}