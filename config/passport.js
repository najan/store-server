const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt-nodejs');

passport.use(new LocalStrategy({
	usernameField: 'emailAddress',
	passportField: 'password'
},function(emailAddress, password, done){
	User.findOne({
		emailAddress: emailAddress
	}, function(err, user){
		if(err){
			return done({message: 'An error occured. Try again Later'});
		}
		if(!user){
			return done(null, false, {message: 'Credentials not found'})
		}
		bcrypt.compare(password, user.password, function(err, res){
			if(!res){

				return done(null, false, ({message: 'Credentials not found'}))
			}
		
		return done(null, user, {message: 'Login Successful'})
		}
		)
	})
}))